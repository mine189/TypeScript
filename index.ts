//SUM
function sum(a:number, b:number):number{
    return a+b;
}

const firstNumber: number=10;
const secondNumber: number=20;

console.log(sum(firstNumber,secondNumber));


// STRING
const myTexVariable: string="Hello";

const myTextOne: string = 'Single quotes text'; // single quotes
const myTextTwo: string = "Double quotes text"; // double quotes
const myTextThree: string = `Grave quotes template ${myTexVariable}`; 


//NUMBER
const myAge: number = 31;           // DECIMAL
const hexFirstNum: number = 0xf010d;     // HEXADECIMAL
const binSecondNum: number = 0b1011;      // BINARY
const octThirdNum: number = 0o744;       // OCTAL

//ANY
let maybeIs: any = 4;
maybeIs = 'a string?';
maybeIs = true;

