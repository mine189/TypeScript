//SUM
function sum(a, b) {
    return a + b;
}
var firstNumber = 10;
var secondNumber = 20;
console.log(sum(firstNumber, secondNumber));
// STRING
var myTexVariable = "Hello";
var myTextOne = 'Single quotes text'; // single quotes
var myTextTwo = "Double quotes text"; // double quotes
var myTextThree = "Grave quotes template ".concat(myTexVariable);
//NUMBER
var myAge = 31; // DECIMAL
var hexFirstNum = 0xf010d; // HEXADECIMAL
var binSecondNum = 11; // BINARY
var octThirdNum = 484; // OCTAL
//ANY
var maybeIs = 4;
maybeIs = 'a string?';
maybeIs = true;
